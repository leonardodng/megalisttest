package com.aplication.megalisttest.api;

import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.models.Computers;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WebApi {

    @GET("list")
    Observable<List<Computer>> getComputers();
}
