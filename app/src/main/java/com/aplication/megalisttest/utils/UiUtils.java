package com.aplication.megalisttest.utils;

import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aplication.megalisttest.R;
import com.google.android.material.snackbar.Snackbar;

import timber.log.Timber;

public class UiUtils {
    public static void handleThrowable(Throwable throwable) {
        Timber.e(throwable, throwable.toString());
    }

    public static void showSnackbar(View view, String message, int length) {
        Snackbar.make(view, message, length).setAction("Action", null).show();
    }

    private static void launchFragmentKeepingInBackStack(Context context, Fragment fragmentToLaunch, String fragmentTag) {
        FragmentManager supportFragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragmentToLaunch, fragmentTag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public static void launchFragmentKeepingInBackStack(Context context, Fragment fragmentToLaunch) {
        launchFragmentKeepingInBackStack(context, fragmentToLaunch, fragmentToLaunch.getClass().getName());
    }

    public static void launchFragmentWithoutKeepingInBackStack(Context context, Fragment fragmentToLaunch) {
        launchFragmentWithoutKeepingInBackStack(context, fragmentToLaunch, fragmentToLaunch.getClass().getName());
    }

    private static void launchFragmentWithoutKeepingInBackStack(Context context, Fragment fragmentToLaunch, String fragmentTag) {
        FragmentManager supportFragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragmentToLaunch, fragmentTag)
                .commitAllowingStateLoss();
    }
}
