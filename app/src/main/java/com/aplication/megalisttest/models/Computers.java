package com.aplication.megalisttest.models;


import com.google.gson.annotations.Expose;

import java.util.List;

public class Computers {
    @Expose
    private List<Computer> elements;

    public List<Computer> getElements() {
        return elements;
    }

    public void setElements(List<Computer> elements) {
        this.elements = elements;
    }
}
