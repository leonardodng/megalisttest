package com.aplication.megalisttest.screen.splash.dagger;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.screen.splash.SplashScreenActivity;
import com.aplication.megalisttest.screen.splash.core.SplashModel;
import com.aplication.megalisttest.screen.splash.core.SplashPresenter;
import com.aplication.megalisttest.screen.splash.core.SplashView;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
@Module
public class SplashModule {

    @SplashScope
    @Provides
    SplashPresenter providePresenter(RxSchedulers schedulers, SplashModel model) {
        CompositeDisposable compositeSubscription = new CompositeDisposable();
        return new SplashPresenter(model, schedulers, compositeSubscription);
    }


    @SplashScope
    @Provides
    SplashView provideSplashView(SplashScreenActivity context) {
        return new SplashView(context);
    }


    @SplashScope
    @Provides
    SplashModel provideSplashModel(WebApi api, SplashScreenActivity ctx) {
        return new SplashModel(api, ctx);
    }
}
