package com.aplication.megalisttest.screen.splash;


import android.content.Intent;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


import com.aplication.megalisttest.R;

import com.aplication.megalisttest.screen.MainActivity;


public class SplashScreenActivity extends AppCompatActivity {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        showComputersListActivity();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // splashPresenter.onDestroy();
    }

    public void showComputersListActivity() {

        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(i);
        finish();

    }




}
