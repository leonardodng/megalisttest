package com.aplication.megalisttest.screen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.aplication.megalisttest.R;
import com.aplication.megalisttest.screen.computers.ComputersListFragment;
import com.aplication.megalisttest.utils.UiUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UiUtils.launchFragmentKeepingInBackStack(this, new ComputersListFragment());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onDestroy();
    }


}
