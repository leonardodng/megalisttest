package com.aplication.megalisttest.screen.computers.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aplication.megalisttest.R;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.models.Computers;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ComputerAdapter extends RecyclerView.Adapter<ComputersViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();
    ArrayList<Computer> listComputers = new ArrayList<>();

    public void swapAdapter(ArrayList<Computer> computers)
    {
        this.listComputers.clear();
        this.listComputers.addAll(computers);
        notifyDataSetChanged();
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }

    @NonNull
    @Override
    public ComputersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_computer, parent, false);
        return new ComputersViewHolder(view ,itemClicks);
    }

    @Override
    public void onBindViewHolder(@NonNull ComputersViewHolder holder, int position) {
        Computer computer = listComputers.get(position);
        holder.bind(computer);
    }

    @Override
    public int getItemCount() {
        if (listComputers != null && listComputers.size() > 0) {
            return listComputers.size();
        } else {
            return 0;
        }
    }
}
