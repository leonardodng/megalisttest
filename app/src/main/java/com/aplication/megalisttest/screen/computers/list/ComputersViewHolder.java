package com.aplication.megalisttest.screen.computers.list;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.aplication.megalisttest.R;
import com.aplication.megalisttest.models.Computer;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

public class ComputersViewHolder extends ViewHolder {
    View view;

    @BindView(R.id.card_view_image)
    ImageView imageComputer;
    @BindView(R.id.card_view_title)
    TextView titleComputer;
    @BindView(R.id.card_view_description)
    TextView descriptionComputer;


    public ComputersViewHolder(View itemView, PublishSubject<Integer> clickSubject) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(v -> clickSubject.onNext(getAdapterPosition())
        );
    }

    void bind(Computer computer) {
        Glide.with(view.getContext())
                .load(computer.getImage())
                .error(R.mipmap.ic_launcher)
                .into(imageComputer);

        titleComputer.setText(TextUtils.isEmpty(computer.getTitle()) ? "missing title" : computer.getTitle());
        descriptionComputer.setText(TextUtils.isEmpty(computer.getDescription()) ? "missing year" : computer.getDescription());

    }

}
