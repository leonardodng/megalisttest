package com.aplication.megalisttest.screen.computers;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aplication.megalisttest.application.AppController;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.MainActivity;
import com.aplication.megalisttest.screen.computers.core.ComputersPresenter;
import com.aplication.megalisttest.screen.computers.core.ComputersView;
import com.aplication.megalisttest.screen.computers.dagger.ComputersModule;
import com.aplication.megalisttest.screen.computers.dagger.DaggerComputersComponent;
import com.aplication.megalisttest.screen.details.ComputerDetailsActivity;

import java.io.Serializable;

import javax.inject.Inject;

public class ComputersListFragment extends Fragment {

    @Inject
    ComputersView view;
    @Inject
    ComputersPresenter presenter;



    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DaggerComputersComponent.builder().appComponent(AppController.getNetComponent()).computersModule(new ComputersModule(this)).build().inject(this);
        presenter.onCreate();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter.getComputersList();
        return view.view();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void goToComputerDetailsActivity(Computer computer) {

        Intent in = new Intent(getActivity(), ComputerDetailsActivity.class);
        in.putExtra("computer", (Serializable) computer);
        startActivity(in);

    }
    @Override
    public void onDetach() {
        super.onDetach();
     onDestroy();
    }
}
