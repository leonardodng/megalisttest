package com.aplication.megalisttest.screen.details.dagger;

import com.aplication.megalisttest.screen.details.ComputerDetailsActivity;

import dagger.Component;

@Component(modules = {ComputerDetailModule.class})
public interface ComputerDetailComponent {
    void inject(ComputerDetailsActivity context);
}
