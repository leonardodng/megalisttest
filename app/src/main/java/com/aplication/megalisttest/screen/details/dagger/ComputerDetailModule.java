package com.aplication.megalisttest.screen.details.dagger;

import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.details.ComputerDetailsActivity;
import com.aplication.megalisttest.screen.details.core.ComputerDetailView;

import dagger.Module;
import dagger.Provides;

@Module
public class ComputerDetailModule {

    ComputerDetailsActivity detailsContext;
    Computer computer;

    public ComputerDetailModule(ComputerDetailsActivity context, Computer computer)
    {
        this.detailsContext = context;
        this.computer = computer;
    }

    @Provides
    ComputerDetailView provideView()
    {
        return  new  ComputerDetailView (detailsContext,computer);
    }
}
