package com.aplication.megalisttest.screen.computers.dagger;

import com.aplication.megalisttest.application.builder.AppComponent;
import com.aplication.megalisttest.screen.computers.ComputersListFragment;

import dagger.Component;

@ComputersScope
@Component(dependencies = {AppComponent.class} , modules = {ComputersModule.class})
public interface ComputersComponent {

    void inject (ComputersListFragment computersActivity);
}
