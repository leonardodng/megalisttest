package com.aplication.megalisttest.screen.computers.core;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.computers.ComputersListFragment;
import com.aplication.megalisttest.utils.NetworkUtils;

import java.util.List;

import io.reactivex.Observable;


public class ComputersModel {

    ComputersListFragment context;
    WebApi api;

    public ComputersModel(ComputersListFragment context, WebApi api) {
        this.context = context;
        this.api = api;
    }

    Observable<List<Computer>> provideListComputers() {
        return api.getComputers();
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context.getActivity());
    }



    public void gotoComputerDetailsActivity(Computer computer) {
        context.goToComputerDetailsActivity(computer);
    }

}
