package com.aplication.megalisttest.screen.computers.dagger;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.screen.computers.ComputersListFragment;
import com.aplication.megalisttest.screen.computers.core.ComputersModel;
import com.aplication.megalisttest.screen.computers.core.ComputersPresenter;
import com.aplication.megalisttest.screen.computers.core.ComputersView;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ComputersModule {

    ComputersListFragment computersListContext;

    public ComputersModule( ComputersListFragment context) {

        this.computersListContext = context;
    }



    @ComputersScope
    @Provides
    ComputersView provideView() {
        return new ComputersView(computersListContext);
    }

    @ComputersScope
    @Provides
    ComputersPresenter providePresenter(RxSchedulers schedulers, ComputersView view, ComputersModel model) {
        CompositeDisposable subscriptions = new CompositeDisposable();
        return new ComputersPresenter(schedulers, model, view, subscriptions);
    }



    @ComputersScope
    @Provides
    ComputersListFragment provideContext() {
        return computersListContext;
    }

    @ComputersScope
    @Provides
    ComputersModel provideModel(WebApi api) {
        return new ComputersModel(computersListContext, api);
    }
}
