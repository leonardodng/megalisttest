package com.aplication.megalisttest.screen.details;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.details.core.ComputerDetailView;
import com.aplication.megalisttest.screen.details.dagger.ComputerDetailModule;
import com.aplication.megalisttest.screen.details.dagger.DaggerComputerDetailComponent;

import javax.inject.Inject;

public class ComputerDetailsActivity extends AppCompatActivity {

    @Inject
    ComputerDetailView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Computer computer = (Computer) getIntent().getExtras().get("computer");

        DaggerComputerDetailComponent.builder().computerDetailModule(new ComputerDetailModule(this, computer)).build().inject(this);

        setContentView(view.view());

    }
}
