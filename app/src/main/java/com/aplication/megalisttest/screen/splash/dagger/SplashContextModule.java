package com.aplication.megalisttest.screen.splash.dagger;

import com.aplication.megalisttest.screen.splash.SplashScreenActivity;

import dagger.Module;
import dagger.Provides;
@Module
public class SplashContextModule {
    SplashScreenActivity splashContext;

    public SplashContextModule(SplashScreenActivity context) {
        this.splashContext = context;
    }

    @SplashScope
    @Provides
    SplashScreenActivity provideSplashContext() {
        return splashContext;
    }
}
