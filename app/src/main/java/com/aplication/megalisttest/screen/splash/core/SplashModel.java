package com.aplication.megalisttest.screen.splash.core;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.models.Computers;
import com.aplication.megalisttest.screen.splash.SplashScreenActivity;
import com.aplication.megalisttest.utils.NetworkUtils;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;

public class SplashModel {

    private WebApi api;
    private SplashScreenActivity splashContext;

    public SplashModel(WebApi api, SplashScreenActivity splashCtx) {
        this.api = api;
        this.splashContext = splashCtx;
    }

    Observable<List<Computer>> provideListComputers(){
        return api.getComputers();
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(splashContext);
    }


    public void gotComputersListActivity() {


    }
}
