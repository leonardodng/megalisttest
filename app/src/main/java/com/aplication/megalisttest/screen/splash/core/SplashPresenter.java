package com.aplication.megalisttest.screen.splash.core;

import android.util.Log;

import com.aplication.megalisttest.utils.UiUtils;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class SplashPresenter {

    private SplashModel model;
    private RxSchedulers rxSchedulers;
    private CompositeDisposable subscriptions;

    public SplashPresenter(SplashModel model, RxSchedulers rxSchedulers, CompositeDisposable subscriptions) {
        this.model = model;
        this.rxSchedulers = rxSchedulers;
        this.subscriptions = subscriptions;
    }

    public void onCreate() {
        subscriptions.add(getComputersList());

    }

    public void onDestroy() {
        subscriptions.clear();
    }
    private Disposable getComputersList() {

        return model.isNetworkAvailable().doOnNext(networkAvailable -> {
            if (!networkAvailable) {
                Log.d("no conn", "no connexion");
            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.isNetworkAvailable()).
                subscribeOn(rxSchedulers.internet()).
                observeOn(rxSchedulers.androidThread()).subscribe(aBoolean -> model.gotComputersListActivity(), throwable -> UiUtils.handleThrowable(throwable));
    }
}
