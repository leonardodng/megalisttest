package com.aplication.megalisttest.screen.computers.core;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aplication.megalisttest.R;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.computers.ComputersListFragment;
import com.aplication.megalisttest.screen.computers.list.ComputerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class ComputersView {
    @BindView(R.id.computer_list_recycleview)
    RecyclerView list;

    View view;
    ComputerAdapter adapter;


    public ComputersView(ComputersListFragment context ) {
        FrameLayout parent = new FrameLayout(context.getActivity());
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context.getActivity()).inflate(R.layout.fragment_computer_list, parent, true);
        ButterKnife.bind(this, view);

        adapter = new ComputerAdapter();

        list.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getActivity());
        list.setLayoutManager(mLayoutManager);


    }

    public Observable<Integer> itemClicks()
    {
        return adapter.observeClicks();
    }

    public View view() {
        return view;
    }

    public void swapAdapter(ArrayList<Computer> computers)
    {
        adapter.swapAdapter(computers);
    }
}
