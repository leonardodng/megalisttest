package com.aplication.megalisttest.screen.splash.dagger;

import com.aplication.megalisttest.application.builder.AppComponent;
import com.aplication.megalisttest.screen.splash.SplashScreenActivity;

import dagger.Component;

@SplashScope
@Component(modules = {SplashContextModule.class, SplashModule.class}, dependencies = {AppComponent.class})
public interface SplashComponent {
    void inject(SplashScreenActivity activity);
}
