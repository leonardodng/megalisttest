package com.aplication.megalisttest.screen.details.core;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aplication.megalisttest.R;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.screen.details.ComputerDetailsActivity;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComputerDetailView {


    @BindView(R.id.computer_image_detail)
    ImageView imageComputer;
    @BindView(R.id.computer_title_text)
    TextView title;
    @BindView(R.id.computer_details_text)
    TextView description;

    View view;

    public ComputerDetailView (ComputerDetailsActivity activity, Computer computer)
    {
        FrameLayout layout = new FrameLayout(activity);
        layout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        view = LayoutInflater.from(activity).inflate(R.layout.activity_computer_detail,layout,true);
        ButterKnife.bind(this,view);

        Glide.with(activity)
                .load(computer.getImage())
                .error(R.mipmap.ic_launcher)
                .into(imageComputer);
        title.setText(TextUtils.isEmpty(computer.getTitle()) ? "no intro" : computer.getTitle());
        description.setText(TextUtils.isEmpty(computer.getDescription()) ? "no text" : computer.getDescription());

    }

    public View view()
    {
        return view;
    }
}
