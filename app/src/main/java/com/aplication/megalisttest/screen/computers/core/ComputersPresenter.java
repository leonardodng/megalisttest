package com.aplication.megalisttest.screen.computers.core;

import android.util.Log;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.models.Computer;
import com.aplication.megalisttest.models.Computers;
import com.aplication.megalisttest.utils.UiUtils;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ComputersPresenter {
    ComputersView view;
    ComputersModel model;
    RxSchedulers rxSchedulers;
    CompositeDisposable subscriptions;
   ArrayList<Computer> comp;
    @Inject
    Retrofit retrofit;

    public ComputersPresenter(RxSchedulers rxSchedulers, ComputersModel model, ComputersView view, CompositeDisposable subscriptions) {
        this.view = view;
        this.model = model;
        this.rxSchedulers = rxSchedulers;
        this.subscriptions = subscriptions;

    }
    public void onCreate() {

        Log.d("enter to presenter", "oki");
        subscriptions.add(getComputersList());
        subscriptions.add(respondToClick());

       /// getComputersList();
    }


    public void onDestroy() {
        subscriptions.clear();
    }


    private Disposable respondToClick() {

        return view.itemClicks().subscribe(integer -> model.gotoComputerDetailsActivity(comp.get(integer)));
    }


    /*private Disposable getComputersList() {

       /* Call<Computers> compus= retrofit.create(WebApi.class).getComputers();

        compus.enqueue(new Callback<Computers>() {
            @Override
            public void onResponse(Call<Computers> call, Response<Computers> response) {
                computers.setElements(response.body().getElements());
            }

            @Override
            public void onFailure(Call<Computers> call, Throwable t) {

            }
        });
        return model.isNetworkAvailable().doOnNext(networkAvailable -> {
            if (!networkAvailable) {
                Log.d("no conn", "no connexion");
                // UiUtils.showSnackbar();
                // Show Snackbar can't use app
            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.provideListComputers()).
                subscribeOn(rxSchedulers.internet()).
                observeOn(rxSchedulers.androidThread()).subscribe(computes -> {
                    Log.d("ok loaded", "cccc");
                    view.swapAdapter((ArrayList<Computer>) computes.getElements());
                    computers = (ArrayList<Computer>) computes.getElements();
                }, throwable -> {
                    UiUtils.handleThrowable(throwable);
                }
        );
        return  view.swapAdapter( computers);
    }*/

    public Disposable getComputersList() {

        return model.isNetworkAvailable().doOnNext(networkAvailable -> {
            if (!networkAvailable) {

            }
        }).
                filter(isNetworkAvailable -> true).
                flatMap(isAvailable -> model.provideListComputers())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(computers -> {

                    view.swapAdapter((ArrayList<Computer>) computers);
                    comp = (ArrayList<Computer>) computers ;
                }, throwable -> {
                    UiUtils.handleThrowable(throwable);
                }
        );

    }


}
