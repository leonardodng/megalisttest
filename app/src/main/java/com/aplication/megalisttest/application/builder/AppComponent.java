package com.aplication.megalisttest.application.builder;

import com.aplication.megalisttest.api.WebApi;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class, RxModule.class, ApiServiceModule.class})
public interface AppComponent {
    RxSchedulers rxSchedulers();
    WebApi apiService();
}
