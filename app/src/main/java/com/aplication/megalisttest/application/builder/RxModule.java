package com.aplication.megalisttest.application.builder;

import com.aplication.megalisttest.utils.rx.AppRxSchedulers;
import com.aplication.megalisttest.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
