package com.aplication.megalisttest.application;

import android.app.Application;


import com.aplication.megalisttest.application.builder.AppComponent;
import com.aplication.megalisttest.application.builder.AppContextModule;
import com.aplication.megalisttest.application.builder.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


import timber.log.BuildConfig;
import timber.log.Timber;

public class AppController extends Application {
    private static AppComponent appComponent;

    private static AppController mInstance;
    private Scheduler scheduler; //Rx object

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
        initialiseLogger();
        initAppComponent();


    }
    public static synchronized AppController getInstance() {
        return mInstance;
    }
    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }
    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }


    private void initialiseLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new Timber.Tree() {
                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    //TODO  decide what to log in release version
                }
            });
        }
    }

    public static AppComponent getNetComponent() {
        return appComponent;
    }

}
